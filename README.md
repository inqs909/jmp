# JMP

This is my presentation on Joint Models. It is based on both of Rizopoulos (2012) book on joint models and Rizopoulos (2010) JM R Package. The presentation is designed for epidemiologists.


## Viewing Presentation

To view this presentation, download the repository and open the JM.html file in a browser. To view presenter notes, press 'p' in the presentation.
